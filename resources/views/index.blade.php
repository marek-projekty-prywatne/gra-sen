<html>
    <head>
        <link rel="stylesheet" href="css/app.css">
    </head>
    <body>
        <div id="app" style="display:flex; width:100%; height:100%; padding:0;margin:0;">
            <div class="panel-container">
                <menu-component></menu-component>
            </div>
            <div class="main-container">
                <div class="game-container">
                    <main-component></main-component>
                </div>
                <div class="log-container">
                    assp[mae]
                </div>
            </div>
        </div>
    </body>
    <script src="js/app.js"></script>
</html>